<?php

    /**
    * 
    */
    class controller
    {
        private $db;
        private $uploaddir;

        function __construct()
        {
            $this->db = new DB();
            $this->uploaddir = $_SERVER['DOCUMENT_ROOT'].'/uploads/';
        }

        private function _is_granted(){
            return ($_SESSION["grant"]!='false');
        }

        public function alert($str, $redirect='/'){
            echo '<script>';
            echo "alert('$str');";
            echo "window.location='$redirect'";
            echo '</script>';
        }

        public function board(){
            include_once "view/header.php";

            $data = $this->db->fetch_posts();
            include_once "view/board.php";
            include_once "view/footer.php";
        }

        public function view($idx){
            $idx = intval($idx);

            if(!$idx) alert('no such post');

            $data = $this->db->fetch_post($idx);
            if($data===NULL){
                $this->alert('no such post');
            }

            include_once "view/header.php";
            include_once "view/view.php";
            include_once "view/footer.php";
        }

        public function write(){
            if(!$this->_is_granted()){
                $this->alert('you need auth', '/auth');
            }

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if(!isset($_POST['content']) || strlen ($_POST['content'])==0) die('no content');
                if(!file_exists($_FILES['img']['tmp_name']) || !is_uploaded_file($_FILES['img']['tmp_name'])) die('no img');


                $file_name = basename($_FILES['img']['tmp_name']).time();
                $uploadfile = $this->uploaddir.$file_name;

                move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile);
                $img = $this->db->add_img($file_name, $_FILES['img']['name'], mime_content_type($uploadfile));
                $this->db->write_post($this->RemoveXSS(strip_tags($_POST['content'])), $img);

                $this->alert('write success');
                return;
            }

            include_once "view/header_none.php";
            include_once "view/write.php";
            include_once "view/footer.php";
        }

        public function auth(){
            if($this->_is_granted()){
                $this->alert('already authenticated', '/');
            }

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if (isset($_POST['passcode'])) {
                    if($_POST['passcode'] === 'b1344gqb3rbq34'){
                        $_SESSION["grant"]='true';
                        $this->alert('authenticated', '/');
                    }
                }
                $this->alert('authenticate fail', '/auth');
                return;
            }

            include_once "view/header_none.php";
            include_once "view/auth.php";
            include_once "view/footer.php";
        }

        public function img($idx){
            $data = $this->db->fetch_img_data($idx);

            if($data===NULL){
                $this->alert('no image');
            }

            header('Content-Disposition: filename='.$data['img_real_name']);
            header('Cache-Control: max-age=94608000, public');
            header('Expires: '.gmdate('D, d M Y H:i:s T', strtotime('+3 years')));
            header('Pragma: cache');

            $file = $this->uploaddir.$data['img_name'];
            $file_size = filesize($file);
            $fp = fopen($file,'r');

            header('Content-Type: '.$data['mime']);
            header('Content-Length: '.$file_size);
            header('Content-Transfer-Encoding: binary');

            echo fread($fp, $file_size);
            fclose($fp);

        }

        public function none(){
            echo '404';
            exit();
        }

        private function RemoveXSS($val) {
            // remove all non-printable characters. CR(0a) and LF(0b) and TAB(9) are allowed
            // this prevents some character re-spacing such as <java\0script>
            // note that you have to handle splits with \n, \r, and \t later since they *are*
            // allowed in some inputs
            $val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
            // straight replacements, the user should never need these since they're normal characters
            // this prevents like <IMG SRC=&#X40&#X61&#X76&#X61&#X73&#X63&#X72&#X69&#X70&#X74&
            // #X3A&#X61&#X6C&#X65&#X72&#X74&#X28&#X27&#X58&#X53&#X53&#X27&#X29>
            $search = 'abcdefghijklmnopqrstuvwxyz';
            $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
            $search .= '1234567890!@#$%^&*()'; 
            $search .= '~`";:?+/={}[]-_|\'\\'; 
            for ($i = 0; $i < strlen($search); $i++) { 
            // ;? matches the ;, which is optional
            // 0{0,7} matches any padded zeros, which are optional and go up to 8 chars
            // &#x0040 @ search for the hex values
            $val = preg_replace('/(&#[x|X]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val);
            // with a ;
            // &#00064 @ 0{0,7} matches '0' zero to seven times
            $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val); // with a ;
            }
            // now the only remaining whitespace attacks are \t, \n, and \r
            $ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style',
            'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
            $ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload'); 
            $ra = array_merge($ra1, $ra2);

            $found = true; // keep replacing as long as the previous round replaced something 
            while ($found == true) {
                $val_before = $val;
                for ($i = 0; $i < sizeof($ra); $i++) {
                    $pattern = '/';
                    for ($j = 0; $j < strlen($ra[$i]); $j++) {
                        if ($j > 0) {
                            $pattern .= '(';
                            $pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
                            $pattern .= '|(&#0{0,8}([9][10][13]);?)?';
                            $pattern .= ')?';
                        }
                        $pattern .= $ra[$i][$j];
                    } 
                    $pattern .= '/i'; 
                    $replacement = substr($ra[$i], 0, 2).'<x>'.substr($ra[$i], 2); // add in <> to nerf the tag
                    $val = preg_replace($pattern, $replacement, $val); // filter out the hex tags
                    if ($val_before == $val) {
                        // no replacements were made, so exit the loop
                        $found = false;
                    }
                }
            }
            return $val;
        }
    }
?>
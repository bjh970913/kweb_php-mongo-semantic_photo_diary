<?php
	include_once "controller.php";
	include_once "model.php";

	$controller = new controller();
	$db = new DB();

	if(isset($_SERVER['REDIRECT_URL'])){
		$route = $_SERVER['REDIRECT_URL'];
	}
	else{
		$route = '/';
	}

	$route = explode('/', $route);

	switch ($route[1]) {
		case 'board':
			$controller->board();
			break;
		case 'view':
			if(!isset($route[2])){
				$controller->none();
			}
			$controller->view($route[2]);
			break;
		case 'img':
			if(!isset($route[2])){
				$controller->none();
			}
			$controller->img($route[2]);
			break;
		case 'write':
			$controller->write();
			break;
		case 'auth':
			$controller->auth();
			break;
		case 'reset':
			$db->db_init();
			break;
		default:
			$controller->board();
			break;
	}
?>
<?php
/**
* 
*/
class DB
{
	private $db;
	private $post;
	
	public function __construct(){
		// $a = new DOMXPath();
		// var_dump($a);
		$this->host = 'localhost';

		$this->db = 'kweb';

		$this->db = new MongoClient("mongodb://$this->host");//, array("username" => $this->user, "password" => $this->pwd, "db" => $this->db));
		$this->db->selectDB($this->db);
		$this->post = $this->db->selectCollection('kweb', 'post');
		$this->img = $this->db->selectCollection('kweb', 'img');
		$this->counter = $this->db->selectCollection('kweb', 'counter');
	}

	public function _get_count($collection){
		$query = array(
			$collection => array(
				'$gte'=>0
			)
		);

		$count = $this->counter->findone($query);
		$count = intval($count[$collection]+1);

		$update = array(
			$collection => $count
		);
		$this->counter->update($query, array(
			'$set' => $update
		));

		return $count;
	}

	public function db_init(){
		$this->post->remove(array());
		$this->img->remove(array());
		$this->counter->remove(array());

		$this->counter->insert(array(
			'post' => 0,
		));

		$this->counter->insert(array(
			'img' => 0,
		));
	}

	public function write_post($content, $img){
		$idx = $this->_get_count('img');
		$data = array(
			'idx' => $idx,
			'content' => $content,
			'img' => $img
		);
		$this->post->insert($data);

		return $idx;
	}

	public function add_img($img_name, $img_real_name, $mime){
		$idx = $this->_get_count('img');
		
		$data = array(
			'idx' => $idx,
			'img_name' => $img_name,
			'img_real_name' => $img_real_name,
			'mime' => $mime
		);
		$this->img->insert($data);

		return $idx;
	}

	public function fetch_posts(){
		$cursor = $this->post->find(array());

		$data=[];

		foreach ($cursor as $key => $value) {
			$data[] = $value;
		}
		return $data;
	}

	public function fetch_post($idx){
		$query = array(
			'idx' => $idx
		);

		return  $this->post->findone($query);
	}

	public function fetch_img_data($idx){
		$query = array(
			'idx' => intval($idx)
		);
		return $this->img->findone($query);
	}
}
?>
<div class="ui link cards">
<?php
	foreach ($data as $key => $value) {
?>
	<div class="ui card">
		<a href="/view/<?php echo $value['idx'];?>">
			<img src="/img/<?php echo $value['img'];?>">
			<div class="ui attached segment">
				<?php echo $value['content'];?>
			</div>
		</a>
	</div>
<?php
	}
?>
</div>
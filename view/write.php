<form action="" method="post" class="ui form" encType="multipart/form-data">
	<div class="ui segments">
		<div class="ui segment field" id="content">
			<textarea class="ui input" type="text" name="content" /></textarea> 
		</div>
		<div class="ui segment field" id="gpic">
			<div class="ui one doubling cards field">
			</div>
			<label>사진 선택</label>
			<div class="field">
				<input class="ui button" type="file" name="img" />
			</div>
		</div>
	</div>
	<div class="field">
		<button type="submit" class="ui blue fluid button">저장</button>
	</div>
</form>
<!DOCTYPE html>
<html>
<head>
	<title>Diary</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/static/semantic/dist/semantic.min.css">
	<script src="/static/semantic/dist/semantic.min.js"></script>
	<meta charset="utf-8">
	<style type="text/css">
		#wrapper{
			width: 100%;
			padding: 30px;

			margin-top: 101px;
		}

		#header{
			width: 100%;
			height: 100px;
			position: absolute;
			top: 0px;

			text-align: center;
			padding: 10px;

			border-bottom: 1px solid #bbbbbb;
		}

		#header a{
			width: 85%;
		}

		.card{
			overflow: hidden;
		}

		img{
			max-width: 300px;
			height: 200px;
		}

		a{
			color: #0b0b0b;
		}
	</style>
</head>
<body>
<div id="header">
	<h2>Diary</h2>
	<a class="ui button blue" href="/write">write post</a>
</div>
<div id="wrapper">
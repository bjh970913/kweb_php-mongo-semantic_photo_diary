# php-mongo-semantic photo diary

## - simple image base blog


### Dependency
- php >=5.x
- mongodb >=3.x

### Pages

- / => main page
- /board => main page
- /view/$1 => main page
- /img/$1 => main page
- /write => post writing page
- /auth => auth page
- /reset => db reset page

### Auth passcode
- b1344gqb3rbq34

### DEMO
- http://52.79.133.245/